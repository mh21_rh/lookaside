#! /bin/bash

# Try to install and start node-exporter, but continue if it fails
sudo dnf install -y node-exporter
sudo systemctl enable --now prometheus-node-exporter

set -euo pipefail
sudo dnf install -y moby-engine
